import Vue from 'vue'
import Router from 'vue-router'
import Find from '@/components/Find'
import Hot from '@/components/Hot'
import New from '@/components/New'
import Search from '@/components/Search'
import Follow from '@/components/Follow'
import Boards from '@/components/Boards'

Vue.use(Router)

function getAbsolutePath () {
  let path = location.pathname
  return path.substring(0, path.lastIndexOf('/') + 1)
}

export default new Router({
  // mode: 'history',
  base: getAbsolutePath (),
  routes: [
    {
      path: '/',
      name: 'Find',
      component: Find
    },
    {
      path: '/hot',
      name: 'Hot',
      component: Hot
    },
    {
      path: '/new',
      name: 'New',
      component: New
    },
    {
      path: '/search',
      name: 'Search',
      component: Search
    },
    {
      path: '/follow',
      name: 'Follow',
      component: Follow
    },
    {
      path: '/boards',
      name: 'Boards',
      component: Boards
    }
  ]
})
